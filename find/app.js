const objects = [{ id: 'a' }, { id: 'b' }];
const found = objects.find(function (item) {
  return item.id === 'a';
});
console.log(found);

const foundIndex = objects.findIndex(function (item) {
  return item.id === 'b';
});
console.log('founded index is '+foundIndex);

